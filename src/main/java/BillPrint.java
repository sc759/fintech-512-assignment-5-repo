import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	final private HashMap<String, Play> plays;
	final private String customer;
	final private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap<>();
		for (Play p: plays) { this.plays.put(p.getId(),p); }

		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	private String usd(int number) {
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		return numberFormat.format((double) number / 100);
	}

	private int totalVolumeCredits(StatementData data) {
		int result = 0;
		for (Performance perf : data.performances) {
			result += perf.getVolumeCredits();
		}
		return result;
	}

	private int totalAmount(StatementData data) {
		int result = 0;
		for (Performance perf: data.performances) {
			result += perf.getAmount();
		}
		return result;
	}

	private String renderPlainText(StatementData data, HashMap<String, Play> plays) {
		String result = "Statement for " + data.customer + "\n";
		for (Performance perf : data.performances) {
			if (perf.getPlay() == null) {
				throw new IllegalArgumentException("No play found");
			}
			result += "  " + perf.getPlay().getName() + ": $" + usd(perf.getAmount()) + " (" + perf.getAudience()
					+ " seats)" + "\n";
		}
		result += "Amount owed is $" + usd(data.totalAmount) + "\n";
		result += "You earned " + data.totalVolumeCredits + " credits" + "\n";
		return result;
	}

	public String statement() {
		return renderPlainText(createStatementData(), plays);
	}

	private StatementData createStatementData() {
		StatementData data = new StatementData(customer, plays);
		data.UpdatePerformance(performances);
		data.totalAmount = totalAmount(data);
		data.totalVolumeCredits = totalVolumeCredits(data);
		return data;
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
