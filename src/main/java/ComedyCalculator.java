public class ComedyCalculator extends PerformanceCalculator{

    public ComedyCalculator(Performance perf, Play play) {
        super(perf, play);
    }

    @Override
    protected void CalcAmount() {
        amount = 30000;
        if (performance.getAudience() > 20) {
            amount += 10000 + 500 * (performance.getAudience() - 20);
        }
        amount += 300 * performance.getAudience();
    }

    @Override
    protected void CalcVolumeCredits() {
        super.CalcVolumeCredits();
        volumeCredits += Math.floor((double) performance.getAudience() / 5.0);
    }
}
