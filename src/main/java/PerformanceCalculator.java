public class PerformanceCalculator {

    final protected Performance performance;
    final protected Play play;
    protected int amount;
    protected int volumeCredits;

    protected PerformanceCalculator(Performance perf, Play play) {
        this.performance = perf;
        this.play = play;
    }

    public static PerformanceCalculator CreatePerformanceCalculator(Performance perf, Play play) {
        return switch (play.getType()) {
            case "tragedy" -> new TragedyCalculator(perf, play);
            case "comedy" -> new ComedyCalculator(perf, play);
            default -> throw new IllegalArgumentException("unknown type: " + play.getType());
        };
    }

    public Play getPlay() {
        return play;
    }

    protected void CalcAmount() {
        throw new RuntimeException("subclass responsibility");
    }

    public int GetAmount() {
        CalcAmount();
        return amount;
    }

    protected void CalcVolumeCredits() {
        volumeCredits = Math.max(performance.getAudience() - 30, 0);
    }

    public int GetVolumeCredits() {
        CalcVolumeCredits();
        return volumeCredits;
    }
}
