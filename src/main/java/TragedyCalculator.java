public class TragedyCalculator extends PerformanceCalculator{

    public TragedyCalculator(Performance perf, Play play) {
        super(perf, play);
    }

    @Override
    protected void CalcAmount() {
        amount = 40000;
        if (performance.getAudience() > 30) {
            amount += 1000 * (performance.getAudience() - 30);
        }
    }
}
