import java.util.ArrayList;
import java.util.HashMap;

public class StatementData {

    public String customer;
    public ArrayList<Performance> performances;
    private final HashMap<String, Play> plays;
    public int totalAmount;
    public int totalVolumeCredits;

    public StatementData(String customer,
                         HashMap<String, Play> plays) {
        this.customer = customer;
        this.plays = plays;
    }

    public void UpdatePerformance (ArrayList<Performance> performances) {
        this.performances = enrichMapping(performances);
    }

    private ArrayList<Performance> enrichMapping(ArrayList<Performance> performances) {
        ArrayList<Performance> result = new ArrayList<>();
        while (performances.size() > 0) {
            Performance perf = performances.remove(0);
            result.add(enrichPerformance(perf));
        }
        return result;
    }

    private Performance enrichPerformance(Performance perf) {
        PerformanceCalculator calculator = PerformanceCalculator.CreatePerformanceCalculator(perf, playFor(perf));
        perf.setPlay(calculator.getPlay());
        perf.setAmount(calculator.GetAmount());
        perf.setVolumeCredits(calculator.GetVolumeCredits());
        return perf;
    }

    private Play playFor(Performance perf) {
        return plays.get(perf.getPlayID());
    }

}
